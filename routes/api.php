<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

$models = [
    (object)['name' => 'droplets', 'controller' => 'Droplet'],
    (object)['name' => 'hosts', 'controller' => 'Host'],
    (object)['name' => 'domains', 'controller' => 'Domain'],
    (object)['name' => 'vhosts', 'controller' => 'Vhost'],
    (object)['name' => 'records', 'controller' => 'Record'],
    (object)['name' => 'users', 'controller' => 'User'],
];

Route::post('login', 'API\LoginController@login')->name('login');
Route::post('register', 'API\LoginController@register')->name('register');

Route::group(['middleware' => 'auth:api'], function() use ($models) {
    Route::get('details', 'API\LoginController@details');

    foreach ($models as $model)
    {
        Route::any($model->name . '/with/{table}', "API\\{$model->controller}Controller@with");
        Route::resource($model->name, "API\\{$model->controller}Controller");
    }

    Route::prefix('digitalocean')->group(function() {
        Route::get('details', 'API\DigitaloceanController@details');
        Route::get('account', 'API\DigitaloceanController@identify');
    });
});
