<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\App;

Route::get('/', function () {
    return view('index');
});

// catch-all -- all routes that are not index or api will be redirected
Route::any('{catchall}', function() {
    return view('index'); // should also redirect to AngularJS
})->where('catchall', '.*');
