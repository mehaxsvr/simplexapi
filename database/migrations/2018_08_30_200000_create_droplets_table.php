<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDropletsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('droplets', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->string('name');
            $table->string('host');
            $table->string('server_ip');
            $table->string('floating_ip')->nullable();
            $table->integer('ssh_port')->nullable();
            $table->string('ssh_user')->nullable();
            $table->string('ssh_pass')->nullable();
            $table->integer('do_id')->unsigned()->nullable();
            $table->timestamp('do_created_at')->nullable();
            $table->integer('vcpus')->unsigned()->nullable();
            $table->integer('memory')->unsigned()->nullable();
            $table->integer('disk')->unsigned()->nullable();
            $table->string('www_dir')->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vhosts');
        Schema::dropIfExists('hosts');
        Schema::dropIfExists('droplets');
    }
}
