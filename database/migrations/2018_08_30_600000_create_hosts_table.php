<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hosts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('droplet_id')->unsigned();
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('record_id')->unsigned();
            $table->string('name');
            $table->string('site_name')->nullable();
            $table->string('www_dir')->nullable();
            $table->string('server_admin')->nullable();
            $table->string('access_log')->nullable();
            $table->string('error_log')->nullable();
            $table->timestamps();

            $table->foreign('droplet_id')->references('id')->on('droplets');
            $table->foreign('record_id')->references('id')->on('records');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vhosts');
        Schema::dropIfExists('hosts');
    }
}
