<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVhostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vhosts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('host_id')->unsigned()->nullable();
            $table->integer('user_id')->unsigned()->nullable();
            $table->string('server_name');
            $table->string('server_alias')->nullable();
            $table->string('server_admin')->nullable();
            $table->string('document_root')->nullable();
            $table->string('error_log')->nullable();
            $table->string('access_log')->nullable();
            $table->timestamps();

            $table->foreign('host_id')->references('id')->on('hosts');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vhosts');
    }
}
