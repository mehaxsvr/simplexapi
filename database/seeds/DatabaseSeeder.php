<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $now = date('Y-m-d H:i:s');

        Db::table('users')->insert([
            [
                'id' => 1,
                'name'  => 'mehax',
                'email' => 'melos.hasanaj@gmail.com',
                'password' => Hash::make('123456'),
                'digitalocean_token' => '7e55336012b505ef0e3d8bd5f2bfedb7f940de85a3a7646bd9e066be28cd4b5e',
                'level' => 1,
                'created_at' => $now
            ],
            [
                'id' => 2,
                'name'  => 'test',
                'email' => 'test@example.com',
                'password' => Hash::make('123456'),
                'digitalocean_token' => null,
                'level' => 0,
                'created_at' => $now
            ],
        ]);

        Db::table('droplets')->insert([
            [
                'id' => 1,
                'user_id' => null,
                'name' => 'mehax-webserver',
                'host' => 'mehax.info',
                'server_ip' => '139.59.155.61',
                'floating_ip' => '206.189.248.6',
                'ssh_port' => 22,
                'ssh_user' => 'root',
                'ssh_pass' => null,
                'vcpus' => 2,
                'memory' => 2,
                'disk' => 40,
                'www_dir' => '/var/www/',
                'created_at' => $now,
            ],
            [
                'id' => 2,
                'user_id' => null,
                'name' => 'mehax-svr',
                'host' => 'db.mehax.info',
                'server_ip' => '46.101.218.89',
                'floating_ip' => '188.166.194.221',
                'ssh_port' => 22,
                'ssh_user' => 'root',
                'ssh_pass' => null,
                'vcpus' => 1,
                'memory' => 1,
                'disk' => 20,
                'www_dir' => '/var/www/',
                'created_at' => $now,
            ],
            [
                'id' => 3,
                'user_id' => null,
                'name' => 'mehax-local',
                'host' => 'local.mehax.info',
                'server_ip' => '185.191.164.79',
                'floating_ip' => null,
                'ssh_port' => 22,
                'ssh_user' => 'root',
                'ssh_pass' => null,
                'vcpus' => 4,
                'memory' => 2,
                'disk' => 250,
                'www_dir' => '/var/www/',
                'created_at' => $now,
            ],
        ]);

        Db::table('domains')->insert([
            [
                'id' => 1,
                'user_id' => null,
                'name' => 'mehax.info',
                'created_at' => $now,
            ],
            [
                'id' => 2,
                'user_id' => null,
                'name' => 'smpx.info',
                'created_at' => $now,
            ],
            [
                'id' => 3,
                'user_id' => null,
                'name' => 'gdgames.info',
                'created_at' => $now,
            ],
            [
                'id' => 4,
                'user_id' => null,
                'name' => 'gonote.info',
                'created_at' => $now,
            ],
            [
                'id' => 5,
                'user_id' => null,
                'name' => 'labshendeti.com',
                'created_at' => $now,
            ],
            [
                'id' => 6,
                'user_id' => null,
                'name' => 'torrentshow.info',
                'created_at' => $now,
            ],
        ]);

        Db::table('records')->insert([
            [ // mehax.info
                'id' => 1,
                'user_id' => null,
                'domain_id' => 1,
                'type' => 'A',
                'name' => '@',
                'data' => '206.189.248.6',
                'created_at' => $now,
            ],
            [ // www.mehax.info
                'id' => 2,
                'user_id' => null,
                'domain_id' => 1,
                'type' => 'CNAME',
                'name' => 'www',
                'data' => 'mehax.info',
                'created_at' => $now,
            ],
            [ // smpx.info
                'id' => 3,
                'user_id' => null,
                'domain_id' => 2,
                'type' => 'A',
                'name' => '@',
                'data' => '188.166.194.221',
                'created_at' => $now,
            ],
            [ // www.smpx.info
                'id' => 4,
                'user_id' => null,
                'domain_id' => 2,
                'type' => 'CNAME',
                'name' => 'www',
                'data' => 'smpx.info',
                'created_at' => $now,
            ],
            [ // torrentshow.info
                'id' => 5,
                'user_id' => null,
                'domain_id' => 5,
                'type' => 'A',
                'name' => '@',
                'data' => '188.166.194.221',
                'created_at' => $now,
            ],
            [ // www.torrentshow.info
                'id' => 6,
                'user_id' => null,
                'domain_id' => 5,
                'type' => 'CNAME',
                'name' => 'www',
                'data' => 'torrentshow.info',
                'created_at' => $now,
            ],
        ]);

        Db::table('hosts')->insert([
            [
                'id' => 1,
                'droplet_id' => 1, // mehax.info
                'record_id' => 1, // mehax.info
                'user_id' => null,
                'name' => 'mehax',
                'www_dir' => 'mehax.info',
                'site_name' => 'mehax.info.conf',
                'server_admin' => 'melos.hasanaj@gmail.com',
                'access_log' => '${APACHE_LOG_DIR}/access.log',
                'error_log' => '${APACHE_LOG_DIR}/error.log',
                'created_at' => $now,
            ],
            [
                'id' => 2,
                'droplet_id' => 2, // db.mehax.info
                'record_id' => 5, // torrentshow.info
                'user_id' => null,
                'name' => 'TorrentShow',
                'www_dir' => 'torrentshow.info',
                'site_name' => 'torrentshow.info.conf',
                'server_admin' => 'melos.hasanaj@gmail.com',
                'access_log' => '${APACHE_LOG_DIR}/access.log',
                'error_log' => '${APACHE_LOG_DIR}/error.log',
                'created_at' => $now,
            ],
            [
                'id' => 3,
                'droplet_id' => 2, // db.mehax.info
                'record_id' => 3, // smpx.info
                'user_id' => null,
                'name' => 'SimpleX',
                'www_dir' => 'smpx.info',
                'site_name' => 'smpx.info.conf',
                'server_admin' => 'melos.hasanaj@gmail.com',
                'access_log' => '${APACHE_LOG_DIR}/access.log',
                'error_log' => '${APACHE_LOG_DIR}/error.log',
                'created_at' => $now,
            ],
        ]);

        Db::table('vhosts')->insert([
            [
                'id' => 1,
                'host_id' => 1, // mehax.info
                'server_name' => 'mehax.info',
                'server_alias' => 'www.mehax.info',
                'document_root' => '/var/www/mehax.info/www',
            ],
            [
                'id' => 2,
                'host_id' => 2, // smpx.info
                'server_name' => 'smpx.info',
                'server_alias' => 'www.smpx.info',
                'document_root' => '/var/www/smpx.info/www',
            ],
            [
                'id' => 3,
                'host_id' => 3, // torrentshow.info
                'server_name' => 'torrentshow.info',
                'server_alias' => 'www.torrentshow.info',
                'document_root' => '/var/www/torrentshow.info/www',
            ],
        ]);
    }
}
