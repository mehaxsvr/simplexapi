<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vhost extends Model
{
    protected $with = ['host'];

    public function host()
    {
        return $this->belongsTo(Host::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
