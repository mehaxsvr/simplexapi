<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Host extends Model
{
    protected $hidden = [];
    protected $with = ['droplet', 'record'];

    public function droplet()
    {
        return $this->belongsTo(Droplet::class);
    }

    public function record()
    {
        return $this->belongsTo(Record::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function vhosts()
    {
        return $this->hasMany(Vhost::class);
    }
}
