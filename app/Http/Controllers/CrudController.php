<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CrudController extends Controller
{
    protected $model;
    protected $fields;
    protected $with = [];
    protected $authorize = false;
    protected $where = [];

    private function addUser()
    {
        if ($this->authorize && Auth::user()->level == 1)
        {
            array_push($this->with, 'user');
        }
    }

    public function index(Request $request) // GET ALL
    {
        $this->addUser();

        $model = $this->model;
        if ($this->authorize && Auth::user()->level == 0)
            $this->where['user_id'] = Auth::user()->id;
        if ($request->input('with'))
        {
            $with = explode(',', $request->input('with'));
            $this->with = array_merge($this->with, $with);
        }
        return response()->success($model::where($this->where)->with($this->with)->get()->all());
    }

    public function filter(Request $request)
    {
        $this->addUser();

        $model = $this->model;
        $with = (isset($request->with) ? explode('|', $request->with) : []);

        $query = $model::where($this->where)->where(function($query) use($request) {
            $fields = $request->toArray();
            foreach ($fields as $key => $value)
            {
                if ($key === 'keyword')
                {
                    $mainFields = ['name'];
                    $query->where(function($query) use($mainFields, $value) {
                        $value = preg_replace('/\s+/', '%', $value);
                        foreach ($mainFields as $column)
                            $query->orWhere($column, 'like', "%$value%");
                    });
                }
                else if (in_array($key, array_merge($this->fields, ['id'])))
                    $query->where($key, $value);
            }
            if ($this->authorize && Auth::user()->level == 0)
            {
                $query->where('user_id', Auth::user()->id);
            }
        })->with(array_merge($this->with, $with));

        return response()->success($query->get()->all());
    }

    public function show(Request $request, $id) // GET ONE
    {
        $this->addUser();

        $model = $this->model;
        $with = (isset($request->with) ? explode('|', $request->with) : []);
        if ($this->authorize && Auth::user()->level == 0)
            $this->where['user_id'] = Auth::user()->id;

        $row = $model::where($this->where)->with(array_merge($this->with, $with))->find($id);

        if ($row)
            return response()->success($row);
        else
            return response()->error();
    }

    public function store(Request $request, $result = false) // POST
    {
        $this->addUser();

        $model = $this->model;
        $with = (isset($request->with) ? explode('|', $request->with) : []);
        $row = new $this->model();
        foreach ($this->fields as $field)
            $row->$field = $request->input($field, null);
        if ($this->authorize)
        {
            if (Auth::user()->level == 1 && $request->input('user_id'))
                $row->user_id = $request->input('user_id');
            else
                $row->user_id = Auth::user()->id;
        }
        $row->save();

        return !$result ? response()->success($model::with(array_merge($this->with, $with))->find($row->id)) : $row;
    }

    public function update(Request $request, $id, $result = false) // PUT ONE
    {
        $this->addUser();

        $model = $this->model;
        $with = (isset($request->with) ? explode('|', $request->with) : []);
        if ($this->authorize && Auth::user()->level == 0)
            $row = $model::where($this->where)->where('user_id', Auth::user()->id)->find($id);
        else
            $row = $model::where($this->where)->find($id);

        if ($row)
        {
            foreach ($this->fields as $field)
                $row->$field = $request->input($field, null);

            if ($this->authorize && Auth::user()->level == 1 && $request->input('user_id'))
                $row->user_id = $request->input('user_id');

            $row->save();
        }

        return !$result ? $row ? response()->success($model::with(array_merge($this->with, $with))->find($row->id)) : response()->error() : $row;
    }

    public function destroy($id) // DELETE ONE
    {
        $model = $this->model;
        if ($this->authorize && Auth::user()->level == 0)
            $model::where($this->where)->where('user_id', Auth::user()->id)->destroy($id);
        else
//            $model::where($this->where)->destroy($id);
            $model::destroy($id); // TODO: CRUD destroy jail
        return response()->success('Done');
    }

    public function with(Request $request, $table)
    {
        $this->addUser();

        $model = $this->model;
        $tables = explode(',', $table);
        $where = array_merge($request->toArray(), $this->where);
        if ($this->authorize && Auth::user()->level == 0)
            $where['user_id'] = Auth::user()->id;
        return response()->success($model::where($where)->with($tables)->with($this->with)->get()->all());
    }
}
