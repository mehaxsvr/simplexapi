<?php

namespace App\Http\Controllers\API;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller
{
    public function login()
    {
        if (Auth::attempt(['email' => request('email'), 'password' => request('password')]))
        {
            $user = Auth::user();
            $data['token'] = $user->createToken('MyApp')->accessToken;
            return response()->success($data);
        }
        else
            return response()->error('Invalid email/password combination', 401);
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'c_password' => 'required|same:password',
        ]);

        if ($validator->fails())
        {
            return response()->error($validator->errors());
        }
        else
        {
            $input = $request->all();
            $input['password'] = Hash::make($input['password']);
            $user = User::create($input);
            $data['token'] = $user->createToken('MyApp')->accessToken;
            $data['name'] = $user->name;
            return response()->success($data);
        }
    }

    public function details()
    {
        $user = Auth::user();
        return response()->success($user);
    }
}
