<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\CrudController;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends CrudController
{
    protected $model = User::class;
    protected $fields = ['name', 'email', 'digitalocean_token'];
    protected $where = ['level' => 0];

    public $successStatus = 200;

    public function __construct()
    {
//        if (Auth::user()->level !== 1)
//        {
//            abort(401);
//            return;
//        }
    }

}
