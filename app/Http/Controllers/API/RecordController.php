<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\CrudController;
use App\Record;

class RecordController extends CrudController
{
    protected $model = Record::class;
    protected $fields = ['do_id', 'domain_id', 'type', 'name', 'data'];
    protected $authorize = true;
}
