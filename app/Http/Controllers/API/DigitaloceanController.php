<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use DigitalOceanV2\DigitalOceanV2;
use GrahamCampbell\DigitalOcean\Facades\DigitalOcean;

class DigitaloceanController extends Controller
{
    public function details()
    {
        $client_id = env("DIGITALOCEAN_CLIENT_ID", null);
        $redirect_url = env('DIGITALOCEAN_REDIRECT_URL', null);
        $response_type = env('DIGITALOCEAN_RESPONSE_TYPE', null);

        $url = "https://cloud.digitalocean.com/v1/oauth/authorize?client_id=$client_id&redirect_uri=$redirect_url&response_type=$response_type";

        $response = [
            'client_id' => $client_id,
            'redirect_url' => $redirect_url,
            'response_type' => $response_type,
            'url' => $url
        ];

        return response()->success($response);
    }

    public function account()
    {
        $account = DigitalOcean::account()->getUserInformation();
        return response()->success($account);
    }

    public function droplets()
    {
        DigitalOceanV2::class;
        $droplets = DigitalOcean::droplets()->getAll();
        return response()->success($droplets);
    }

    public function domains()
    {
        $domains = DigitalOcean::domains()->getAll();
        return response()->success($domains);
    }
}
