<?php

namespace App\Http\Controllers\API;

use App\Droplet;
use App\Http\Controllers\CrudController;

class DropletController extends CrudController
{
    protected $model = Droplet::class;
    protected $fields = ['name', 'host', 'server_id', 'floating_ip', 'vcpus', 'memory', 'disk', 'ssh_port', 'ssh_user', 'ssh_pass', 'do_id', 'www_dir', 'server_admin', 'access_log', 'error_log'];
    protected $authorize = true;
}
