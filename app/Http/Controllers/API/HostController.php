<?php

namespace App\Http\Controllers\API;

use App\Host;
use App\Http\Controllers\CrudController;

class HostController extends CrudController
{
    protected $model = Host::class;
    protected $fields = ['name', 'droplet_id', 'record_id', 'site_name', 'www_dir', 'server_admin', 'document_root', 'server_admin', 'access_log', 'error_log'];
    protected $authorize = true;
    protected $with = ['vhosts'];
}
