<?php

namespace App\Http\Controllers\API;

use App\Vhost;
use App\Http\Controllers\CrudController;

class VhostController extends CrudController
{
    protected $model = Vhost::class;
    protected $fields = ['host_id', 'server_name', 'server_alias', 'server_admin', 'document_root', 'error_log', 'access_log'];
    protected $authorize = true;
    protected $with = [];
}
