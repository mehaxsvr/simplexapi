<?php

namespace App\Http\Controllers\API;

use App\Domain;
use App\Http\Controllers\CrudController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DomainController extends CrudController
{
    protected $model = Domain::class;
    protected $authorize = true;
    protected $fields = ['name', 'ttl'];
}
