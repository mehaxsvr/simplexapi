<?php

namespace App\Providers;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        \Response::macro('success', function($data = []) {
            return response()->json(['status' => 'ok', 'result' => $data], 200);
        });

        \Response::macro('error', function($message = 'Not found', $status = 404) {
            return response()->json(['status' => 'error', 'message' => $message], $status);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
