<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Droplet extends Model
{
    protected $hidden = [];
    protected $with = ['record'];

    public function record()
    {
        return $this->belongsTo(Record::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
